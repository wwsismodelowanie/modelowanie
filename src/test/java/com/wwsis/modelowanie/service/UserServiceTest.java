package com.wwsis.modelowanie.service;

import com.wwsis.modelowanie.dto.CreateUserDto;
import com.wwsis.modelowanie.dto.UserDto;
import com.wwsis.modelowanie.model.User;
import com.wwsis.modelowanie.repository.UserRepository;
import com.wwsis.modelowanie.service.exceptions.UserAlreadyExists;
import com.wwsis.modelowanie.service.exceptions.UserNotFound;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.internal.verification.VerificationModeFactory.atLeast;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {
    @Mock
    private UserRepository userRepository;
    private UserDtoMapper userDtoMapper = new UserDtoMapper();
    @Captor
    private ArgumentCaptor<User> userArgumentCaptor;

    private UserService userService;

    @Before
    public void init() {
        userService = new UserService(userRepository, userDtoMapper);
    }

    @Test
    public void getByLogin_shouldReturnCorrectDto() throws UserNotFound {
        // given
        User user = new User("john", "qwerty", "john@yahoo.com", "John", "Smith", LocalDate.of(1884, 3,2),"USER", new ArrayList<>());
        Optional<User> userOptional = Optional.of(user);

        // when
        when(userRepository.findById("john")).thenReturn(userOptional);
        UserDto userDto = userService.getByLogin("john");

        // then
        assertEquals("john", userDto.getLogin());
        assertEquals("john@yahoo.com", userDto.getMail());
        assertEquals("John", userDto.getFirstName());
        assertEquals("Smith", userDto.getLastName());
        assertEquals("USER", userDto.getRole());
    }

    @Test(expected = UserNotFound.class)
    public void getByLogin_shouldThrowUserNotFound() throws UserNotFound {
        when(userRepository.findById("john")).thenReturn(Optional.empty());

        userService.getByLogin("john");
    }

    @Test
    public void createUser_shouldHashPasswordBeforeSave() throws UserAlreadyExists {
        // given
        String password = "admin";
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        CreateUserDto createUserDto = new CreateUserDto("admin", password, "admin@wp.pl", "John", "Smith");

        // when
        when(userRepository.save(userArgumentCaptor.capture())).thenReturn(new User());
        userService.createUser(createUserDto);

        User userPassedToRepository = userArgumentCaptor.getValue();

        String hashedPassword = userPassedToRepository.getPassword();

        // sprawdzenie czy hashedPassword to poprawny hash hasła "admin"
        boolean isHashCorrect = bCryptPasswordEncoder.matches(password, hashedPassword);
        assertTrue(isHashCorrect);
    }
}