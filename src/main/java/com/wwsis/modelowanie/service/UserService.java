package com.wwsis.modelowanie.service;

import com.wwsis.modelowanie.dto.CreateUserDto;
import com.wwsis.modelowanie.dto.UpdateUserDto;
import com.wwsis.modelowanie.dto.UserDto;
import com.wwsis.modelowanie.model.User;
import com.wwsis.modelowanie.repository.UserRepository;
import com.wwsis.modelowanie.service.exceptions.UserAlreadyExists;
import com.wwsis.modelowanie.service.exceptions.UserNotFound;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class UserService {
        private final UserRepository userRepository;
        private final UserDtoMapper userDtoMapper;

        @Value("${config.adminDefaultLogin}")
        private String defaultAdminUsername;
        @Value("${config.adminDefaultPass}")
        private String defaultAdminPass;

        /*
            Metoda dodająca domyślnego admina jeśli nie istnieje
            @PostConstruct - wykonuje się przy starcie aplikacji
         */
        @PostConstruct
        public void init(){
            Optional<User> userOptional = userRepository.findById(defaultAdminUsername);
            if(!userOptional.isPresent()){
                String hashedPass = new BCryptPasswordEncoder().encode(defaultAdminPass);
                User newUser = new User(defaultAdminUsername, hashedPass, "","","", LocalDate.now(),"ADMIN",new ArrayList<>());
                userRepository.save(newUser);
            }

        }

        public List<UserDto> getAll(){
            return userRepository.findAll()
                    .stream()
                    .map(u -> userDtoMapper.toDto(u))
                    .collect(Collectors.toList());
        }

        public UserDto getByLogin(String login) throws UserNotFound {
            return userRepository.findById(login)
                    .map(u -> userDtoMapper.toDto(u))
                    .orElseThrow(() -> new UserNotFound());
//
//        Optional<User> userOptional = userRepository.findById(login);
//        if(userOptional.isPresent()){
//            return userDtoMapper.toDto(userOptional.get());
//        } else {
//            throw new UserNotFound();
//        }
        }

        public UserDto createUser(CreateUserDto createUserDto) throws UserAlreadyExists {
            if(userRepository.existsById(createUserDto.getLogin())){
                throw new UserAlreadyExists();
            }

            // "hashowanie" hasła z użyciem BCrypt
            String encodedPassword = new BCryptPasswordEncoder().encode(createUserDto.getPassword());
            createUserDto.setPassword(encodedPassword);

            User user = userDtoMapper.fromDto(createUserDto);
            user.setRole("USER");

            User savedUser = userRepository.save(user);

            return userDtoMapper.toDto(savedUser);
        }

        public UserDto updateUser(String login, UpdateUserDto updateUserDto) throws UserNotFound {
            Optional<User> userOptional = userRepository.findById(login);

            if(userOptional.isPresent()){
                User user = userOptional.get();

                user.setFirstName(updateUserDto.getFirstName());
                user.setLastName(updateUserDto.getLastName());
                user.setMail(updateUserDto.getMail());
                String encodedPassword = new BCryptPasswordEncoder().encode(user.getPassword());
                user.setPassword(encodedPassword);

                User savedUser = userRepository.save(user);
                return userDtoMapper.toDto(savedUser);
            } else {
                throw new UserNotFound();
            }
        }

        public UserDto delete(String login) throws UserNotFound {
            Optional<User> userOptional = userRepository.findById(login);

            if(userOptional.isPresent()){
                User user = userOptional.get();
                userRepository.delete(user);
                return userDtoMapper.toDto(user);
            } else {
                throw new UserNotFound();
            }
        }
}
