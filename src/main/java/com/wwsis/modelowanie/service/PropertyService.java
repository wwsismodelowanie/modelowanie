package com.wwsis.modelowanie.service;

import com.wwsis.modelowanie.dto.CreatePropertyDto;
import com.wwsis.modelowanie.dto.CreateUserDto;
import com.wwsis.modelowanie.dto.PropertyDto;
import com.wwsis.modelowanie.dto.UserDto;
import com.wwsis.modelowanie.model.Property;
import com.wwsis.modelowanie.model.User;
import com.wwsis.modelowanie.repository.PropertyRepository;
import com.wwsis.modelowanie.repository.UserRepository;
import com.wwsis.modelowanie.service.exceptions.PropertyAllreadyExists;
import com.wwsis.modelowanie.service.exceptions.UserAlreadyExists;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class PropertyService {
    private final PropertyRepository propertyRepository;
    private final PropertyDtoMapper propertyDtoMapper;
    private final UserRepository userRepository;


    public List<PropertyDto> getByUser(String userLogin){
        return propertyRepository.findAll().stream()
                .filter(property -> property.getUser().getLogin().equals(userLogin))
                .map(propertyDtoMapper::toDto).collect(Collectors.toList());

/*        return propertyRepository.findByUserLogin(userLogin).stream()
                .map(propertyDtoMapper::toDto).collect(Collectors.toList());*/
    }

    public PropertyDto createProperty(CreatePropertyDto createPropertyDto, String userName) throws PropertyAllreadyExists {
        if(propertyRepository.existsById(createPropertyDto.getName())){
            throw new PropertyAllreadyExists();
        }


        Property property = propertyDtoMapper.toModel(createPropertyDto);
        property.setUser(userRepository.findById(userName).get());
        Property savedProperty = propertyRepository.save(property);

        return propertyDtoMapper.toDto(savedProperty);
    }
}
