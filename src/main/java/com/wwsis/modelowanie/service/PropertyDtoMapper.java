package com.wwsis.modelowanie.service;


import com.wwsis.modelowanie.dto.CreatePropertyDto;
import com.wwsis.modelowanie.dto.CreateUserDto;
import com.wwsis.modelowanie.dto.PropertyDto;
import com.wwsis.modelowanie.dto.UserDto;
import com.wwsis.modelowanie.model.Property;
import com.wwsis.modelowanie.model.Tenant;
import com.wwsis.modelowanie.model.User;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.ArrayList;

@Component
public class PropertyDtoMapper {

    public PropertyDto toDto(Property property) {
        return new PropertyDto(property.getName(), property.getDescription(),
                property.getArea(), property.getPrice(), property.getUser().getLogin());
    }

    public Property toModel(CreatePropertyDto createPropertyDto) {
        return new Property(createPropertyDto.getName(), createPropertyDto.getDescription(),
                createPropertyDto.getArea(), createPropertyDto.getPrice(), null, new ArrayList<>());
    }
}
