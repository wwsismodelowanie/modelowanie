package com.wwsis.modelowanie.service;

import com.wwsis.modelowanie.dto.CreateUserDto;
import com.wwsis.modelowanie.dto.UserDto;
import com.wwsis.modelowanie.model.User;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.ArrayList;

@Component
public class UserDtoMapper {
    public UserDto toDto(User user) {
        return new UserDto(user.getLogin(), user.getMail(), user.getFirstName(), user.getLastName(), user.getRole());
    }

    public User fromDto(CreateUserDto createUserDto) {
        return new User(createUserDto.getLogin(), createUserDto.getPassword(), createUserDto.getMail(),
                createUserDto.getFirstName(), createUserDto.getLastName(), LocalDate.now(), null, new ArrayList<>());
    }
}
