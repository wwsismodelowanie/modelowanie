package com.wwsis.modelowanie.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Property {
    @Id
    private String name;
    private String description;
    private float area;
    private float price;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_login", nullable = false)
    private User user;

    @OneToMany(mappedBy = "property", cascade = CascadeType.ALL)
    private List<Tenant> tenants;


}
