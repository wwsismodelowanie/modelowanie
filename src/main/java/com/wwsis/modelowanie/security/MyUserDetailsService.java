package com.wwsis.modelowanie.security;

import com.wwsis.modelowanie.model.User;
import com.wwsis.modelowanie.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class MyUserDetailsService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;


    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        Optional<User> userOptional = userRepository.findById(login);

        if (userOptional.isPresent()) {
            // nasz znaleziony użytkownik (wyciągnięty z bazy danych)
            User user = userOptional.get();

            // zwrócenie danych użytkownika jako model ze SpringSecurity
            return org.springframework.security.core.userdetails.User
                    .withUsername(login)
                    .authorities("ROLE_" + user.getRole().toUpperCase())
                    .password(user.getPassword())
                    .build();
        } else {
            throw new UsernameNotFoundException("User does not exist!");
        }
    }
}

