package com.wwsis.modelowanie.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Profile("!local")
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private MyUserDetailsService myUserDetailsService;

    @Autowired
    public void configureGlobalSecurity(AuthenticationManagerBuilder auth) throws Exception {
//        auth.inMemoryAuthentication()
//                .withUser("user").password("{noop}user").roles()
//                .and()
//                .withUser("editor").password("{noop}editor").roles("EDITOR")
//                .and()
//                .withUser("admin").password("{noop}admin").roles("ADMIN");

//        auth.userDetailsService(myUserDetailsService);


        // ustawienie jako źródła danych serwisu MyUserDetailsService oraz hashowania haseł z użyciem BCrypt
        DaoAuthenticationProvider dap = new DaoAuthenticationProvider();
        dap.setUserDetailsService(myUserDetailsService);
        dap.setPasswordEncoder(new BCryptPasswordEncoder());
        auth.authenticationProvider(dap);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable() // wyłączenie zabezpieczenia przed CSRF
                .authorizeRequests() // autoryzuj wszystkie żądania
                .antMatchers("/owner/**").hasAnyRole("OWNER", "ADMIN")
                .antMatchers("/admin/**").hasAnyRole("ADMIN") // tylko użytkownik z rolą ADMIN ma dostęp do /admin/**
                .antMatchers("/api/**").hasAnyRole("ADMIN")
                .antMatchers("/register").permitAll() // dopuść do adresu /register wszystkich, nawet niezalogowanych
                .antMatchers("/").permitAll()
                .antMatchers("/contact").permitAll()
                .antMatchers("/css/**").permitAll()
                .antMatchers("/img/**").permitAll()
                .antMatchers("/vendor/**").permitAll()
                .anyRequest().authenticated() // wszystkie pozostałe zapytania dostępne tylko dla zalogowanych
                .and()
                .formLogin()
                .loginPage("/login") // strona wyświetlana jako formularz logowania, jednocześnie
                // adres na jaki mają trafiać POSTem dane logowania
                .usernameParameter("login")  // nazwa parametru z nazwą użytkownika
                .passwordParameter("password") // nazwa parametru z hasłem
                .permitAll()
                .and()
                .logout(); // dodanie wylogowania pod adresem /logout
    }
}
