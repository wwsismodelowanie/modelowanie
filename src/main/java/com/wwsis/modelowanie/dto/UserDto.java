package com.wwsis.modelowanie.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserDto {
    private String login;
    private String mail;
    private String firstName;
    private String lastName;
    private String role;
}
