package com.wwsis.modelowanie.dto;

import com.wwsis.modelowanie.model.Tenant;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PropertyDto {
    private String name;
    private String description;
    private float area;
    private float price;
    private String userLogin;
}
