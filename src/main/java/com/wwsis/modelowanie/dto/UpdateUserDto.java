package com.wwsis.modelowanie.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UpdateUserDto {
    @Size(min = 4)
    @NotNull
    private String password;
    @NotNull
    @Email
    private String mail;
    @NotBlank
    private String firstName;
    @NotBlank
    private String lastName;
}
