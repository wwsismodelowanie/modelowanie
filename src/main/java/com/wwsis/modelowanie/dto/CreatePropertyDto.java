package com.wwsis.modelowanie.dto;

import com.wwsis.modelowanie.model.Tenant;
import com.wwsis.modelowanie.model.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreatePropertyDto {
    @Size(min = 4)
    @NotNull
    private String name;
    private String description;
    private float area;
    private float price;
    private String user;
}
