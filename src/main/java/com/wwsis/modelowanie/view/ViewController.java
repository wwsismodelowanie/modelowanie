package com.wwsis.modelowanie.view;

import com.wwsis.modelowanie.dto.*;
import com.wwsis.modelowanie.service.PropertyService;
import com.wwsis.modelowanie.service.UserService;
import com.wwsis.modelowanie.service.exceptions.PropertyAllreadyExists;
import com.wwsis.modelowanie.service.exceptions.UserAlreadyExists;
import com.wwsis.modelowanie.service.exceptions.UserNotFound;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

@Controller
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ViewController {
    private final UserService userService;
    private final PropertyService propertyService;

    @GetMapping({"/", "", "/dupa"})
    public ModelAndView home() {

        ModelAndView modelAndView = new ModelAndView("index");

        return modelAndView;
    }


    @GetMapping("/admin/users")
    public ModelAndView usersPage() {
        List<UserDto> users = userService.getAll();

        ModelAndView modelAndView = new ModelAndView("users");
        modelAndView.addObject("users", users);
        return modelAndView;
    }

    @GetMapping("/owner/properties")
    public ModelAndView properties(Authentication authentication) {
        List<PropertyDto> properties = propertyService.getByUser(authentication.getName());

        ModelAndView modelAndView = new ModelAndView("properties");
        modelAndView.addObject("properties", properties);
        return modelAndView;
    }

    @GetMapping("/owner/addProperty")
    public ModelAndView addPostView() {
        ModelAndView modelAndView = new ModelAndView("createProperty");

        CreatePropertyDto createPostDto = new CreatePropertyDto();
        modelAndView.addObject("createPropertyDto", createPostDto);

        return modelAndView;
    }

    @PostMapping("/owner/addProperty")
    public String properties(@Valid @ModelAttribute CreatePropertyDto createPropertyDto, BindingResult bindingResult, Authentication authentication) throws PropertyAllreadyExists, UserNotFound {
        if (bindingResult.hasErrors()) {
            return "/owner/createProperty";
        }
        PropertyDto property = propertyService.createProperty(createPropertyDto, authentication.getName());

        ModelAndView modelAndView = new ModelAndView("property");
        modelAndView.addObject("property", property);
        return "redirect:/owner/addProperty";
    }

    @GetMapping("/editProfile")
    public String editProfile(Authentication authentication, Model model) throws UserNotFound {
        String username = authentication.getName();
        UserDto userDto = userService.getByLogin(username);

        UpdateUserDto updateUserDto = new UpdateUserDto("", userDto.getMail(), userDto.getFirstName(), userDto.getLastName());
        model.addAttribute("updateUserDto", updateUserDto);
        return "edit_profile";
    }

    @PostMapping("/editProfile")
    public String editProfile(@Valid @ModelAttribute UpdateUserDto updateUserDto, BindingResult bindingResult, Authentication authentication) throws UserNotFound {
        if (bindingResult.hasErrors()) {
            return "edit_profile";
        }
        userService.updateUser(authentication.getName(), updateUserDto);
        return "redirect:/editProfile";
    }


    @GetMapping("/register")
    public ModelAndView displayRegisterPage() {
        CreateUserDto createUserDto = new CreateUserDto();

        ModelAndView modelAndView = new ModelAndView("register");
        modelAndView.addObject("createUserDto", createUserDto);
        return modelAndView;
    }

    @PostMapping("/register")
    public String register(@Valid @ModelAttribute CreateUserDto createUserDto, BindingResult bindingResult) throws UserAlreadyExists {

        if (bindingResult.hasErrors()) {
            return "register";
        }

        userService.createUser(createUserDto);

        return "redirect:/login";
    }

    @GetMapping("/login")
    public ModelAndView displayLoginPage(@RequestParam(required = false) String error) {
        boolean loginError = error != null;

        ModelAndView modelAndView = new ModelAndView("login");
        modelAndView.addObject("loginError", loginError);

        return modelAndView;
    }


    @GetMapping({"/contact"})
    public ModelAndView contact() {

        ModelAndView modelAndView = new ModelAndView("contact");

        return modelAndView;
    }

}
