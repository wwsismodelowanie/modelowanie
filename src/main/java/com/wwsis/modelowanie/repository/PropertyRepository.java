package com.wwsis.modelowanie.repository;

import com.wwsis.modelowanie.model.Property;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

@Repository
public interface PropertyRepository  extends CrudRepository<Property, String> {
    List<Property> findAll();

    @Query("SELECT p FROM Property p WHERE p.user = ?1")
    List<Property> findByUserLogin(String userLogin);
}
